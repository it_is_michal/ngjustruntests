import { Component, OnInit } from '@angular/core';
import { TodoIdGeneratorService } from '../todo-id-generator.service';

@Component({
  selector: 'app-todo-card',
  templateUrl: './todo-card.component.html',
  styleUrls: ['./todo-card.component.css']
})
export class TodoCardComponent implements OnInit {
  public todoId: number;

  constructor(private todoIdgenerator: TodoIdGeneratorService) {
  }

  ngOnInit() {
    this.todoId = this.todoIdgenerator.getNextTodoId();
  }

}
