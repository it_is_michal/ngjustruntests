/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { TodoCardComponent } from './todo-card.component';
import { TodoIdGeneratorService } from '../todo-id-generator.service';

describe('Component: TodoCard', () => {
  let stubbedTodoId = 1337;
  let fixture;
  let service;

  beforeEach(() => {
    let serviceStub = {
        getNextTodoId: ()=> stubbedTodoId
    }

    TestBed.configureTestingModule({
      declarations: [ TodoCardComponent ],
      providers: [ {provide: TodoIdGeneratorService, useValue: serviceStub} ]
    });

    service = TestBed.get(TodoIdGeneratorService);

    fixture = TestBed.createComponent(TodoCardComponent);
  });

  it('should have Todo id', () => {
    fixture.detectChanges();
    expect(service.getNextTodoId()).toEqual(stubbedTodoId, 'stubbed servide should be stubbed');
    expect(fixture.componentInstance.todoId).toEqual(stubbedTodoId, 'the component should use stubbed service');
  });
});
