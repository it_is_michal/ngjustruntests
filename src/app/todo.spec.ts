/* tslint:disable:no-unused-variable */

import { async, inject } from '@angular/core/testing';
import {Todo} from './todo';

describe('Todo', () => {
  it('should create an instance', () => {
    expect(new Todo(12)).toBeTruthy();
  });
  it('should have an id', () => {
    let todo = new Todo(12);
    expect(todo.id).not.toBeNull();
  });
});
