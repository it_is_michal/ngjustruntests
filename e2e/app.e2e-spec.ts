import { JustRunTestsPage } from './app.po';

describe('just-run-tests App', function() {
  let page: JustRunTestsPage;

  beforeEach(() => {
    page = new JustRunTestsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
