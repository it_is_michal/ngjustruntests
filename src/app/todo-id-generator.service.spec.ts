/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TodoIdGeneratorService } from './todo-id-generator.service';

describe('Service: TodoIdGenerator', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TodoIdGeneratorService]
    });
  });

  it('should exist', inject([TodoIdGeneratorService], (service: TodoIdGeneratorService) => {
    expect(service).toBeTruthy();
  }));

  it('should provide next Todo Id', inject([TodoIdGeneratorService], (service: TodoIdGeneratorService) => {
    expect(service.getNextTodoId()).toBeTruthy();
  }));
});
